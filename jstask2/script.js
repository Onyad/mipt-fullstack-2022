const fs = require('fs');

function checkForUpdates(path, cache) {
    const current = fs.readdirSync(path)

    const new_files = current.filter(x => !cache.includes(x))
    const removed_files = cache.filter(x => !current.includes(x))

    for (const file of new_files) {
        console.log(`Created ${file}`)
    }
    for (const file of removed_files) {
        console.log(`Removed ${file}`)
    }

    return current
}

function main() {
    if (process.argv.length != 3) {
        console.log("Expected nodejs script.js {path to directory}");
    }

    const path = process.argv[2]
    let cache = fs.readdirSync(path)

    setInterval(() => {
        cache = checkForUpdates(path, cache)
    }, 1000)
}

if (require.main === module) {
    main()
}
