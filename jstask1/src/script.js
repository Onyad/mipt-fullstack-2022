window.onload = () => {
    checkboxes = document.querySelectorAll('input[type=checkbox]')
    for (checkbox of checkboxes) {
        coffee = checkbox.id
        const img = document.querySelector('#' + coffee + '_img')
        img.hidden = true

        checkbox.onclick = (event) => {
            img.hidden = !img.hidden
        }
    }

    document.querySelector('#submit').onclick = () => {
        location.reload()
    }
}